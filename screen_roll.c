/* x是横轴像素位置，y是页，offset是从当前字符第n个像素开始显示，ch是ascii码 */
void ICACHE_FLASH_ATTR oled12864_i2c_8x16_ch(unsigned char x, unsigned char y, 
                                                        unsigned char offset, unsigned char ch)
{
    unsigned char show_len = 8;
    unsigned char i, c = ch - 32;

    if (x >= 128 || y >= 8)
    {
        return;
    }

    if (x > 120)
    {
        show_len = 128 - x + offset;
    }

    oled12864_i2c_set_pos(x, y);
    for(i = offset; i < show_len; i++)
        oled12864_i2c_write_data(F8X16[c * 16 + i]);

    oled12864_i2c_set_pos(x, y + 1);
    for(i = offset; i < show_len; i++)
        oled12864_i2c_write_data(F8X16[c * 16 + i + 8]);
}

/* one_hz是汉字字符串 */
void ICACHE_FLASH_ATTR oled12864_i2c_16x16_hz1(unsigned char x, unsigned char y, 
                                                        unsigned char offset, unsigned char * one_hz)
{
    unsigned char show_len = 16;
    unsigned char * hz_ecode;
    unsigned char i = 0;

    if (x > 112)
    {
        show_len = 128 - x + offset;
    }

    hz_ecode = font_gb2312_get(one_hz);             /* 获取当前汉字的gb2312编码信息，2字节（1个字） */
    if (NULL == hz_ecode)
    {
        oled12864_i2c_set_pos(x, y);
        for(i = offset; i < show_len; i++)
            oled12864_i2c_write_data(F16x16err[i]);
            
        oled12864_i2c_set_pos(x, y + 1);
        for(i = offset; i < show_len; i++)
            oled12864_i2c_write_data(F16x16err[i + 16]);
        return;
    }

    oled12864_i2c_set_pos(x, y);
    for(i = offset; i < show_len; i++)
        oled12864_i2c_write_data(hz_ecode[i]);
        
    oled12864_i2c_set_pos(x, y + 1);
    for(i = offset; i < show_len; i++)
        oled12864_i2c_write_data(hz_ecode[i + 16]);
}


/* 文字偏移显示，hzstr是汉字和字符的混合字符串 */
/* 使用形式：oled12864_i2c_16x16_roll_hzs(0, 0, 8, "1234你好世界!!!") */
/* 上面offset是8,会从第8个像素开始显示，就是从2开始往后显示 */
bool ICACHE_FLASH_ATTR oled12864_i2c_16x16_roll_hzs(unsigned char x, unsigned char y,  
                                                                unsigned int offset, unsigned char *hzstr)
{
    unsigned int bytes = offset / 8;
    unsigned int i = 0;
    unsigned int piex;

    /* 1、找到字符串需要显示的第一个字节位置 */
    while(i < bytes)
    {
        if (hzstr[i] >= 0xA1)    /* 偏移到GB2312编码区 */
        {
            if (bytes - i == 1)
            {
                break;
            }
            i+=2;
        }
        else if (hzstr[i] >= 0x20 && hzstr[i] <= 0x7f)   /* 偏移到ascii区 */
        {
            i++;
        }
        else return false; /* 偏移到其他区不处理 */
    }

    /* 2、第一个字(或字节) 的偏移量 */
    if (hzstr[i] >= 0xA1)                           /* 当前位置是中文汉字，gb2312汉字编码每个字节都大于0xa1 */
    {
        piex = offset - i * 8;
        oled12864_i2c_16x16_hz1(x, y, piex, hzstr + i);
        x += (16 - piex);
        i += 2;
    }
    else if (hzstr[i] >= 0x20 && hzstr[i] <= 0x7f)   /* 当前位置是ascii字节 */
    {
        piex = offset - i * 8;
        oled12864_i2c_8x16_ch(x, y, piex, hzstr[i]);
        x += (8 - piex);
        i++;
    }
    else return false;                              /* 偏移到其他区不处理 */

    /* 3、从第二个完整字(或字节) 开始往后显示，直到屏幕最右边 */
    while(hzstr[i] != 0)
    {
        if (x >= 128)
        {
            break;
        }

        if (hzstr[i] >= 0x20 && hzstr[i] <= 0x7f)       /* 如果是ascii调用ascii接口显示 */
        {
            oled12864_i2c_8x16_ch(x, y, 0, hzstr[i]);
            x+=8;
            i+=1;
        }
        else if (hzstr[i] >= 0xA1)
        {
            oled12864_i2c_16x16_hz1(x, y, 0, hzstr + i);    /* 如果是汉字调用汉字显示接口显示 */
            x += 16;
            i+=2;
        }
        else break;
    }

    return true;
}

